***Settings***

Documentation       Resource referente aos testes do chatbot Intelbrás

Library             SeleniumLibrary


***Variable***

${URL_CHAT_BOT}     https://chat.blip.ai/?appKey=dGVzdGVwcmF0aWNvNTo4NGYwZmNiZS1lNzE3LTQ0MTctODBiNi1lZTcxNTJhYTgzZjU=



***Keywords***

Abrir navegador
    Open Browser          about:blank     chrome  
    Set Window Size       1360            768

Acessar plataforma take blip
    Abrir navegador
    Go To                                 ${URL_CHAT_BOT} 
    Wait Until Element Is Visible      id=blip-send-message      5
    Capture Page Screenshot
    

Digitar saudação Olá
    Input text           id=msg-textarea      Olá 
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                3

Selecionar opção
    [Arguments]         ${opção}
    Input text           id=msg-textarea      ${opção}
    Click Element        id=blip-send-message
    Wait Until Page Does Not Contain Element                xpath=(//div[@class="typing-dot"])[2]
    Sleep      3

Selecionar opção 2 
    Input text           id=msg-textarea      2
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                9 
   

Digitar modelo RF1200
    Input text           id=msg-textarea      RF1200
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2

Digitar nome da Organização    
    Input text           id=msg-textarea      ORG
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2

Digitar CNPJ    
    Input text           id=msg-textarea      03.938.055/0002-16
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2

Digitar nome do responsável    
    Input text           id=msg-textarea      João
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2

Digitar o telefone do responsável  
    Input text           id=msg-textarea      48 991698889
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2    

Digitar nome do usuário
    Input text           id=msg-textarea      Nelson
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2     

Digitar o e-mail
    Input text           id=msg-textarea      email@email.com
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2     

Digitar telefone do usuário 
    Input text           id=msg-textarea      45 991698888
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                2      

Digitar telefone inválido do usuário     
    Input text           id=msg-textarea      488
    Sleep                1
    Click Element        id=blip-send-message
    Sleep                1

Fechar navegador 
    Close Browser



Validar resposta '${resposta_bot}'
    Page Should Contain Element    //*[@class="blip-container mt3"]/child::div[position()=last()]//*/div//*[contains(text(),'${resposta_bot}')]

Validar pergunta '${pergunta_bot}' 
     Page Should Contain Element     //*[@class="blip-container mt3"]/child::div[position()=last()]//*/div//*[contains(text(),'${pergunta_bot}')]

Validar a opção '${opção_bot}'
    Page Should Contain Element   //*[@class="blip-container mt3"]/child::div[position()=last()]/div[@class='blip-card-group blip-container--with-photo left']//*/ul/li//*[contains(text(),'${opção_bot}')]     