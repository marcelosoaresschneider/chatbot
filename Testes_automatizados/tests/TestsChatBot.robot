***Settings***

Documentation           Suite de testes chatbot Intelbrás

Resource                ../resources/ResourceChatBot.robot

Test Setup                              Acessar plataforma take blip

Test Teardown        Run Keywords       Capture Page Screenshot
...                                     Fechar navegador            

***Test Case***

Realizar fluxo 01
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        0
        Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'
		
		
Realizar fluxo 02
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1    
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        Validar a opção ' Voltar para inicio'
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção        1
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção        1
        Validar resposta 'Você é de um provedor de acesso a internet?'
        Validar a opção '1. Sim, sou de um provedor de Internet.'
        Validar a opção ' Não, eu sou cliente de um provedor de internet'
        Validar a opção '0. Voltar'
        Selecionar opção        1
        Validar resposta 'Escreva o nome de quais modelos que você possuir que estão inoperantes.(RF 1200, RG1200...) e quantidade. Ou'
        Digitar modelo RF1200
        Validar resposta 'Precisamos de mais algumas informações sobre seu provedor.'
        #Validar pergunta 'Nome da organização'
        Digitar nome da organização
        Validar pergunta 'CNPJ do provedor'
        Digitar CNPJ
        Validar pergunta 'responsáveis pelo provedor'
        Digitar nome do responsável
        Validar pergunta 'telefone com DDD'
        Digitar o telefone do responsável 
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'


Realizar fluxo 03
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        #Validar a opção ' Voltar para inicio
        Selecionar opção        1  
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'      
        Selecionar opção        1
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção        1
        Validar resposta 'Você é de um provedor de acesso a internet?'
        Validar a opção '1. Sim, sou de um provedor de Internet.'
        Validar a opção ' Não, eu sou cliente de um provedor de internet'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'	


Realizar fluxo 04
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção         1  
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        #Validar a opção ' Voltar para inicio      
        Selecionar opção         1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção         1
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar resposta ' Detectamos na manhã de sábado, 03/10/2020, um ataque onde os modelos de roteadores '
        Selecionar opção         1	
        Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'


Realizar fluxo 05
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1   
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        #Validar a opção ' Voltar para inicio     
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção        1
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar resposta ' Detectamos na manhã de sábado, 03/10/2020, um ataque onde os modelos de roteadores '
        Selecionar opção 2
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'


Realizar fluxo 06
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1   
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        #Validar a opção ' Voltar para inicio     
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção        1
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar resposta ' Detectamos na manhã de sábado, 03/10/2020, um ataque onde os modelos de roteadores '
        Selecionar opção        3
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'	


Realizar fluxo 07
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action?'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1   
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        #Validar a opção ' Voltar para inicio     
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção 2
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção        1
        Validar resposta 'Você é de um provedor de acesso a internet?'
        Validar a opção '1. Sim, sou de um provedor de Internet.'
        Validar a opção ' Não, eu sou cliente de um provedor de internet'
        Validar a opção '0. Voltar'
        Selecionar opção        1
        Validar resposta 'Escreva o nome de quais modelos que você possuir que estão inoperantes.(RF 1200, RG1200...) e quantidade. Ou'
        Digitar modelo RF1200
        Validar resposta 'Precisamos de mais algumas informações sobre seu provedor.'
        #Validar pergunta 'Nome da organização'
        Digitar nome da organização
        Validar pergunta 'CNPJ do provedor'
        Digitar CNPJ
        Validar pergunta 'responsáveis pelo provedor'
        Digitar nome do responsável
        Validar pergunta 'telefone com DDD'
        Digitar o telefone do responsável 
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'
		
		
Realizar fluxo 08
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action? Não sabe? '
        Validar pergunta 'Então confirme na etiqueta embaixo do produto.'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1    
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        Validar a opção ' Voltar para inicio'
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção 2
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção        1
        Validar resposta 'Você é de um provedor de acesso a internet?'
        Validar a opção '1. Sim, sou de um provedor de Internet.'
        Validar a opção ' Não, eu sou cliente de um provedor de internet'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima' 		
		
		
Realizar fluxo 09
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action? Não sabe? '
        Validar pergunta 'Então confirme na etiqueta embaixo do produto.'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1    
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        Validar a opção ' Voltar para inicio'
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção 2
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar resposta ' Detectamos na manhã de sábado, 03/10/2020, um ataque onde os modelos de roteadores '
        Selecionar opção 2
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima' 


Realizar fluxo 10
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action? Não sabe? '
        Validar pergunta 'Então confirme na etiqueta embaixo do produto.'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção        1    
        Validar pergunta 'Show, gosto muito desse produto, mas me conta, em que posso ajudar?'
        Validar a opção ' Meu roteador não está funcionando'
        Validar a opção ' Quero alterar as configurações do meu roteador.'
        Validar a opção ' Voltar para inicio'
        Selecionar opção        1
        Validar resposta 'Hum, acredito que já sei o que ocorreu no seu roteador, mas me confirme algumas informações, por favor!'
        Validar pergunta 'Preciso que você verifique quais LEDs estão fixos em azul!'
        Validar a opção '1. LEDs “Sys,LAN,WAN”'
        Validar a opção '2. LEDs “Wi-fi,1,2,3,Internet”'
        Validar a opção '3. Nenhuma das opções anteriores'
        Selecionar opção 2
        Validar resposta 'Confirmei aqui, acho que seu produto está inoperante devido ao ataque cibernético que identificamos recentemente. Mas não se preocupe, eu te darei uma solução.'
        Validar pergunta 'Só preciso de algumas informações, pode ser?'
        Validar a opção '1. Ok'
        Validar a opção ' Já passei as informações, só gostaria de saber como me proteger!'
        Validar a opção '0. Voltar'
        Selecionar opção 2
        Validar resposta ' Detectamos na manhã de sábado, 03/10/2020, um ataque onde os modelos de roteadores '	
        Selecionar opção        3
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'	


Realizar fluxo 11
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action? Não sabe? '
        Validar pergunta 'Então confirme na etiqueta embaixo do produto.'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção 2       
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone do usuário
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'


Realizar fluxo 12
        Digitar saudação Olá
        Validar resposta 'Olá, eu sou assistente virtual da Intelbras e estou aqui para ajudar com a Linha Action!'
        Validar pergunta 'Conta ai, você possui algum produto da linha Action? Não sabe? '
        Validar pergunta 'Então confirme na etiqueta embaixo do produto.'
        Validar a opção '1. Sim, tenho roteadores Action.'
        Validar a opção '2. Não, possuo outro modelo'
        Validar a opção '0. Finalizar atendimento'
        Selecionar opção 2       
        Validar pergunta 'digite seu nome' 
        Digitar nome do usuário
        Validar pergunta 'Talvez eu terei que enviar o status da sua solicitação. Por isso, preciso que você informe seu e-mail.'
        Digitar o e-mail
        #Validar pergunta 'Digite seu número de telefone.'
        Digitar telefone inválido do usuário 
        Digitar telefone do usuário 
        Validar resposta 'Já encaminhei sua solicitação para nossa equipe e iremos entrar em contato para mais detalhes.'
        #Validar resposta 'Obrigado pelo seu contato e até breve! #IntelbrasSemprePróxima'	
